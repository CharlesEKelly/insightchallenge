package challenge.structures;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

/**
 * Graph that is used for rolling average degree calculations.
 * <P>
 * GraphStream maintains counters for nodes and edges, which are used for rolling average degree calculations.
 * 
 * 
 * @author Charles Kelly
 *
 */
public class TwitterGraph
{
	private Graph twitterGraph = new SingleGraph("Twitter Hashtags");
	
	private LinkedList<TwitterNode> twitterNodeLinkedList = new LinkedList();	// time slice data structure
	private TwitterNodeComparator twitterNodeComparator = new TwitterNodeComparator();	// maintains order within time slice data structure
	
	private Date maxTime = new Date(0);		// upper boundary for time slice; 31 December 1969, before Twitter "was born"
	private Date minTime = new Date(0);		// lower boundary for time slice
	
	public void addVertices(TwitterData twitterData) throws Exception
	{
		/**
		 * data structure for finding permutations of nodes that will become part of the graph
		 */
		List<Node> edgePermutationList = new ArrayList<Node>();
		try
		{
			List<String> hashtagList = twitterData.getHashtagsList();
			for (String hashtagString : hashtagList)
			{
				Node twitterNode = twitterGraph.getNode(hashtagString);
				if (null == twitterNode)
				{
					Date expirationDate = twitterData.getExpirationDate();
					
					twitterNode = twitterGraph.addNode(hashtagString);
					twitterNode.setAttribute("creationDate", twitterData.getCreationDate() );
					twitterNode.setAttribute("expirationDate", expirationDate );
					twitterNode.setAttribute("id", hashtagString);					
					
					// maintain data structure for "60 second time slice"
					TwitterNode timingNode = new TwitterNode(expirationDate, hashtagString);
					twitterNodeLinkedList.add(timingNode);
				}
				
			nodePermutationLoop:
				for (Node needEdgeNode: edgePermutationList)
				{
					String edgeName = hashtagString + "_" + needEdgeNode.getAttribute("id");
					if (checkForExistingEdge(twitterNode, needEdgeNode))											
						continue nodePermutationLoop;
					
					Edge edge = twitterGraph.getEdge(edgeName);
					if (null != edge)
						continue nodePermutationLoop;
					twitterGraph.addEdge(edgeName, twitterNode, needEdgeNode);
				}
				
				edgePermutationList.add(twitterNode);
			}
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}
	
	/**
	 * Maintain 60 second time slice for tweets
	 * 
	 */
	public void updateTimeSlice(Date referenceDate) throws Exception
	{
		ArrayList<TwitterNode> toRemove = new ArrayList<TwitterNode>();
		try
		{
			if (referenceDate.before(minTime))
				return;
			if (referenceDate.after(maxTime))
			{
				maxTime = referenceDate;
				minTime = DateUtils.addSeconds(referenceDate, -60);
			}
			twitterNodeLinkedList.sort(twitterNodeComparator);
			Iterator<TwitterNode> twitterNodeIterator = twitterNodeLinkedList.iterator();
		iterationLoop:
			while (twitterNodeIterator.hasNext() )
			{
				TwitterNode twitterNode = twitterNodeIterator.next();
				int comparison = referenceDate.compareTo(twitterNode.getExpirationDate() );
				if (comparison > 0)
				{
					String idString = twitterNode.getNodeId();
					twitterGraph.removeNode(idString);
					toRemove.add(twitterNode);
				}
				else
				{
					break iterationLoop;	//list is sorted, therefore no more expired nodes
				}
			}
			twitterNodeLinkedList.removeAll(toRemove);
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}
	
	private boolean checkForExistingEdge(Node node1, Node node2) throws Exception
	{
		try
		{
			Edge edge12 = node1.getEdgeBetween(node2);
			Edge edge21 = node2.getEdgeBetween(node1);	// this should be redundant
			
			boolean bool1 = null != edge12;
			boolean bool2 = null != edge21;
			
			boolean existenceFlag = bool1 || bool2;
			
			return existenceFlag;
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}
	
	public int getEdgeCount()
	{
		return twitterGraph.getEdgeCount();
	}
	
	public int getNodeCount()
	{
		return twitterGraph.getNodeCount();
	}
}
