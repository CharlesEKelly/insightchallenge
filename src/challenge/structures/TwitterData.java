package challenge.structures;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * This class extracts hashtags and data-time information from tweets in JSON format.
 * 
 * 
 * @author Charles Kelly
 *
 */
public class TwitterData
{
	// data format used within input file for tweet information
	public static DateFormat twitterDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss Z yyyy");
	
	private List<String> hashtagsList = new ArrayList<String>();
	private Date creationDate;
	private Date expirationDate;
	private boolean validNodeFlag;		// has 2 or more hashtags
	

	public TwitterData()
	{
		
	}
	
	public TwitterData(Date creationDate, List<String> hashtagsList)
	{
		this.setCreationDate(creationDate);
		this.hashtagsList = hashtagsList;
	}
	
	/**
	 * 
	 * @param jsonString input format used in tweets.txt
	 * @return data structure used in <code>TwitterGraph</code>
	 * @throws Exception
	 */
	public static TwitterData createFromJson(String jsonString) throws Exception
	{
		TwitterData twitterData = new TwitterData();
		try
		{
			JSONObject jsonObject = (JSONObject)new JSONParser().parse(jsonString);
			Object dateObject = jsonObject.get("created_at");
			Date creationDate =twitterDateFormat.parse(dateObject.toString() );
			twitterData.setCreationDate(creationDate);
			
			List<String> hashtagsList = parseHashtags(jsonObject.get("entities").toString() );
			twitterData.setHashtagsList(hashtagsList);
			
			int listSize = hashtagsList.size();
			
			if (listSize > 1)
				twitterData.setValidNodeFlag(true);
			
			return twitterData;
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}
	
	private static List<String> parseHashtags (String hashtagsString) throws Exception
	{
		List<String> hashtagsList = new ArrayList<String>();
		try
		{
			JSONObject jsonObjectEntities = (JSONObject)new JSONParser().parse(hashtagsString);
			JSONArray hashtagsJSONArray = (JSONArray) jsonObjectEntities.get("hashtags");
			int arraySize = hashtagsJSONArray.size();
			if (0 < arraySize)				
			{
				for (int j=0; j<arraySize; j++)
				{
					Object hashtagsObject = hashtagsJSONArray.get(j);
					JSONObject jsonObject3 = (JSONObject)new JSONParser().parse(hashtagsObject.toString());
					Object textValue = jsonObject3.get("text");
					hashtagsList.add(textValue.toString() );
				}
			}
			
			return hashtagsList;
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}

	public List<String> getHashtagsList()
	{
		return hashtagsList;
	}

	public void setHashtagsList(List<String> hashtagsList)
	{
		this.hashtagsList = hashtagsList;
	}

	public Date getCreationDate()
	{
		return creationDate;
	}

	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
		this.expirationDate = DateUtils.addSeconds(creationDate, 60);
	}

	public Date getExpirationDate()
	{
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate)
	{
		this.expirationDate = expirationDate;
	}

	public boolean isValidNodeFlag()
	{
		return validNodeFlag;
	}

	public void setValidNodeFlag(boolean validNodeFlag)
	{
		this.validNodeFlag = validNodeFlag;
	}
	
}
