package challenge.structures;

import java.util.Comparator;
import java.util.Date;

/**
 * Data structure for maintaining 60 second time slice of tweet data
 * 
 * @author Charles Kelly
 *
 */
public class TwitterNode
{
	private Date expirationDate;
	private String nodeId;
	
	public TwitterNode()
	{
		
	}
	
	public TwitterNode(Date expiration_time, String nodeId)
	{
		this.expirationDate = expiration_time;
		this.nodeId = nodeId;
	}
	
	public Date getExpirationDate()
	{
		return expirationDate;
	}



	public void setExpirationDate(Date expirationDate)
	{
		this.expirationDate = expirationDate;
	}



	public String getNodeId()
	{
		return nodeId;
	}



	public void setNodeId(String nodeId)
	{
		this.nodeId = nodeId;
	}

}
