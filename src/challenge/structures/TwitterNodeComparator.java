package challenge.structures;

import java.util.Comparator;

/**
 * Maintain order within data structure for maintaining 60 second time slice of tweet data
 * 
 * @author Charles Kelly
 *
 */
public class TwitterNodeComparator implements Comparator<TwitterNode>
{

	@Override
	public int compare(TwitterNode arg0, TwitterNode arg1)
	{
		int comparison = arg0.getExpirationDate().compareTo(arg1.getExpirationDate());
		
		return comparison;
	}

}
