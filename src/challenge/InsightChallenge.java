package challenge;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;

import challenge.structures.TwitterData;
import challenge.structures.TwitterGraph;

/**
 * This class is the entry point for the Insight Data Engineering Coding Challenge.
 * <P>
 * The main method accesses input file which contain sample tweets and the output file which contain calculated rolling average degree(s).
 * <P>
 * This class contains methods for calculating and formatting rolling average degree.
 * 
 * 
 * @author Charles Kelly
 *
 */
public class InsightChallenge
{
	public static final String FILE_SEPARATOR = System.getProperty ("file.separator");
		
	private static String limitString = "{\"limit";
	
	private static TwitterGraph twitterGraph = new TwitterGraph();
	
	private static BufferedReader bufferedReader = null;
	private static PrintWriter printWriter;
	
	private static int tweetCounter = 0;
	
	public static void main(String[] args)
	{
		System.out.println("Calculations for Insight Coding Challenge");
		
		Double rollingAverageDegree = 0.0;
		
        try
		{
        	setupFiles(); // input and output files
			
        	String line;
		readLoop:	
		    while ( (null != (line = bufferedReader.readLine()))) 
		    {
		    	if (line.startsWith(limitString))
		    		continue readLoop;
		    	
		    	tweetCounter ++;	// used to test that number of rolling average calculations equals the number of input tweets
		    	TwitterData twitterData = TwitterData.createFromJson(line);
		    	Date creationDate = twitterData.getCreationDate();
		    	twitterGraph.updateTimeSlice(creationDate);	// possibly evict nodes from the graph
		        
		        if (twitterData.isValidNodeFlag() )	// does tweet contain 2 or more hashtags?
		        {
					twitterGraph.addVertices(twitterData);
		        } 
		        rollingAverageDegree = calculateRollingAverageDegree();
				System.out.println("rolling average degree: " + rollingAverageDegree);
				writeRollingAverageDegree(rollingAverageDegree);
		    }
			
			
			bufferedReader.close();
			printWriter.close();
			System.out.println ("Tweet counter: " + tweetCounter);
		} 
        catch (Exception e)
		{
			System.out.println(e.toString() );
		}
	}
	
	private static void setupFiles() throws Exception
	{
		try
		{
			File tweetsInputFile = getTweetsInputFile();
        	bufferedReader = new BufferedReader(new FileReader(tweetsInputFile));
        	
        	File tweetsOutputFile = getTweetsOutputFile();
        	printWriter = new PrintWriter(new FileWriter(tweetsOutputFile));
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}
	
	/**
	 * use "present working directory" as starting point for directory search for this code, libraries, and input/output files
	 * 
	 */
	private static File getTweetsInputFile() throws Exception
	{
		try
		{
			File pwdFile = new java.io.File( "." );
        	String pwdFilePath = pwdFile.getCanonicalPath();
            
            File tweetInputFile = new java.io.File(pwdFilePath + FILE_SEPARATOR +  "tweet_input" + FILE_SEPARATOR + "tweets.txt");
            boolean existsFlag = tweetInputFile.exists();
            
            if (! existsFlag)
            	throw new Exception (tweetInputFile.toString() + " does not exist");
            
            return tweetInputFile;
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}
	
	private static File getTweetsOutputFile() throws Exception
	{
		try
		{
			File pwdFile = new java.io.File( "." );
        	String pwdFilePath = pwdFile.getCanonicalPath();
            
            File tweetOutputFile = new java.io.File(pwdFilePath + FILE_SEPARATOR +  "tweet_output" + FILE_SEPARATOR + "output.txt");
            boolean existsFlag = tweetOutputFile.exists();
            
            if (existsFlag)
            	tweetOutputFile.delete();
            
            return tweetOutputFile;
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}
	
	private static void writeRollingAverageDegree(Double rollingAverageDegree) throws Exception
	{
		try
		{
			DecimalFormat df = new DecimalFormat("#####0.00");
			df.setRoundingMode(RoundingMode.DOWN);	// truncate, don't round
			String doubleString = df.format(rollingAverageDegree);

			printWriter.println(doubleString);
			printWriter.flush();
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}
	
	/**
	 * 
	 * Assumption: it is not necessary to iterate across all nodes.  See readme.txt
	 */
	private static Double calculateRollingAverageDegree() throws Exception
	{
		try
		{
			Double nodeCount = new Double (twitterGraph.getNodeCount() );			
			Double edgeCount = new Double (twitterGraph.getEdgeCount() );			
			Double rollingAverageDegree = 2.0 * edgeCount / nodeCount;
			
			if (Double.isNaN(rollingAverageDegree))
				return 0.0;
			
			return rollingAverageDegree;
		} 
		catch (Exception e)
		{
			// catch for debugging
			throw new Exception (e);
		}
	}
	
	

}
