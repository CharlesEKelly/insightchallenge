Introduction
Charles Kelly prepared the code and files for the Insight Data 
Engineering Coding challenge.  The code/solution relies upon 
libraries available from Apache, Google, and GraphStream.

Libraries
The �lib� directory contains the following jar files:
	commons-lang (used for DateUtils)
	gs-algo, gs-core, gs-ui (used for GraphStream)
	json-simple (used for parsing JSON)
This directory also contains:
	Challenge.jar (code written for the challenge)
The run.sh (and run.bat) files use the �-cp� option of the java 
command to find these libraries.

Java compiler
The code was compiled with Oracle�s Java 8 compiler, and tested 
using Java 8 runtime.

Assumptions
If the rolling average degree calculation is performed when 
there are no nodes in the graph, the value 0.00 is written to 
output.txt.  This condition occurs when one or more tweets at 
the beginning of the tweets.txt file contain fewer than two 
hashtags.  (This condition occurs in the generated tweets.txt 
file that is distributed with the challenge.)

The code uses the following equation to calculate the rolling 
average degree:

	Double rollingAverageDegree = 2.0 * edgeCount / nodeCount;

The code maintains counters for the total number of edges and 
nodes in the graph (rather than iterating across all nodes each 
time the average calculation is performed).
It is possible that this equation is not correct for all cases.  
In a production environment, either discussion with a domain 
expert, or empirical testing, would expose the error and the 
less efficient iteration could be used.  (The assumption is 
correct for the data in the test suite.)

Production Architecture
The submitted code maintains the graph in memory.  (This was 
sufficient for the 9295 tweets contained in the generated tweets 
file.)  In production the graph could be stored in a distributed 
memory cache, or in a graph database (such as Neo4j).

Build Process
In production a build system such as Maven or Gradle could be 
used for compiling, testing, and running the code.  The build 
system could be used in conjunction with a continuous 
integration server.  The submitted code does not make any 
assumptions about build systems that are available on the 
machine used to test the code.

Testing the Code
You should not need to download any libraries; needed libraries 
are contained within �lib� directory.  The run.sh (and run.bat) 
file calls the java runtime to execute �InsightChallenge.class�.
The code prints each calculated rolling average degree to the 
console while the code is running.  (The correct format is 
written to output.txt file).  In a production environment, it is 
easy to remove the console output.

